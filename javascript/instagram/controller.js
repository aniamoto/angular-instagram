angular.module('InstaSearch')
  .controller("InstagramController", ['$scope', 'InstagramFactory', 
    function ($scope, InstagramFactory) {
      $scope.letterLimit = 140;
      $scope.search = function () {
        if (!$scope.searchForm.$valid) {
          return;
        }
        var tag = $scope.searchField;
        $scope.images = null;
        $scope.searchForm.$setPristine();
        //$scope.searchField = "";
        InstagramFactory.get(21, tag).success(function (result) {
          if (result.meta.code == 200) {
            $scope.images = result.data;
          } else {
            $scope.error = "No results.";
          }
        }).error(function() {
          $scope.error = "Connection problem.";
        });
      };
    }
  ]);

