angular.module('InstaSearch')
  .factory("InstagramFactory", ['$http',
    function($http) {
      var base = "https://api.instagram.com/v1",
          clientId = '642176ece1e7445e99244cec26f4de1f';
      return {
        'get': function(count, tag) {
          var request = '/tags/' + tag + '/media/recent',
              url = base + request,
              config = {
                'params': {
                  'client_id': clientId,
             	  'count': count,
              	  'callback': 'JSON_CALLBACK'
                }
       	      };
       	  return $http.jsonp(url, config);
        }
      }
    }
  ]);

